using ResourceString.Net.Logic.Factories;

namespace ResourceString.Net.Logic.Tests;

[TestClass]
public class LogicResourcesTests
{

    [TestMethod]
    public void Test_Get_ResourceStringMembers()
    {
        var rm = Properties.Resources.ResourceManager;
        Assert.AreEqual(
            new FormattedResourceString(
                Properties.Resources.ResourceStringMembers.Format,
                default,
                LiteralString.Factory("ResourceStringMembers"),
                LiteralString.Factory("ResourceManager")
            ).Value,
            string.Format(
                rm.GetString("ResourceStringMembers") ?? string.Empty,
                "ResourceStringMembers",
                "ResourceManager"
            )
        );
    }

    [TestMethod]
    public void Test_Get_ResourceFormatClassMembers()
    {
        var rm = Properties.Resources.ResourceManager;
        Assert.AreEqual(
            new FormattedResourceString(
                Properties.Resources.ResourceFormatClassMembers.Format,
                default,
                LiteralString.Factory("ResourceFormatClassMembers"),
                LiteralString.Factory("ResourceManager"),
                LiteralString.Factory("Format")
            ).Value,
            string.Format(
                rm.GetString("ResourceFormatClassMembers") ?? string.Empty,
                "ResourceFormatClassMembers",
                "ResourceManager",
                "Format"
            )
        );
    }

    [TestMethod]
    public void Test_Get_ResourceFormatClassFromMethod()
    {
        var rm = Properties.Resources.ResourceManager;
        Assert.AreEqual(
            new FormattedResourceString(
                Properties.Resources.ResourceFormatClassFromMethod.Format,
                default,
                LiteralString.Factory("IResourceString name"),
                LiteralString.Factory("name")
            ).Value,
            string.Format(
                rm.GetString("ResourceFormatClassFromMethod") ?? string.Empty,
                "IResourceString name",
                "name"
            )
        );
    }

    [TestMethod]
    public void Test_Get_ResourceManagerMemberTemplate()
    {
        var rm = Properties.Resources.ResourceManager;
        Assert.AreEqual(
            CodeSnippetFactory.CreateResourceMangerMemberCodeSnippet(
                "Resources",
                "Resources"
            ).Value,
            string.Format(
                rm.GetString("ResourceManagerMemberTemplate") ?? string.Empty,
                "Resources",
                "Resources"
            )
        );
    }

    [TestMethod]
    public void Test_Get_ResourcesClassTemplate()
    {
        var rm = Properties.Resources.ResourceManager;

        var rmSnippet = CodeSnippetFactory.CreateResourceMangerMemberCodeSnippet(
            "Resources",
            "Resources"
        );

        Assert.AreEqual(
            CodeSnippetFactory.CreateResourceClassCodeSnippet(
                "ResourceString.Net.Logic.Properties",
                "Resources",
                rmSnippet,
                Enumerable.Empty<IResourceString>()
            ).Value,
            string.Format(
                rm.GetString("ResourcesClassTemplate") ?? string.Empty,
                "ResourceString.Net.Logic.Properties",
                "Resources",
                rmSnippet.Value,
                string.Empty
            )
        );
    }
}

