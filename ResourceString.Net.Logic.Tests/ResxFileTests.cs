using ResourceString.Net.Logic.DomainObjects.Resx;
using ResourceString.Net.Logic.Factories;
using ResourceString.Net.Logic.Parsers.Resx;

namespace ResourceString.Net.Logic.Tests;

[TestClass]
public class ResxFileTests
{
    private readonly string validXml = @$"<?xml version='1.0' encoding='utf-8'?>
<root>
  <data name='Test1'>
    <value>Value1</value>
    <comment>This is a greeting message.</comment>
  </data>
  <data name='Test2' type='{typeof(string).FullName}'>
    <value>{{2}}Value2{{0}}{{{{1}}}}</value>
    <comment>2 = prefix</comment>
  </data>
  <data name='Test3' type='{typeof(byte[]).FullName}'>
    <value>0xDEADBEEF</value>
  </data>
</root>";

    private const string expectedClass = @"
using ResourceString.Net.Contract;
using System;
using System.Globalization;
using System.Resources;
using System.Threading;

namespace TestNameSpace
{
    internal static class TestResourceClass
    {

        #region ResourceManager 

        private static readonly Type _Type = typeof(TestResourceClass);
      
        private static readonly Lazy<ResourceManager> _ResourceManager = new Lazy<ResourceManager>(
            () => new ResourceManager(""ResourceManager"" ?? string.Empty, _Type.Assembly),                   
            LazyThreadSafetyMode.PublicationOnly
        );

        public static ResourceManager ResourceManager => _ResourceManager.Value;

        private static CultureInfo GetDefaultCulture()
        {
            return CultureInfo.CurrentCulture;
        }

        private static IResourceString AddToCultureCache(IResourceString source)
        {
            return new CultureBasedCachedString(source, GetDefaultCulture);
        }

        #endregion // ResourceManager
    

        #region Test1 

        private static readonly Lazy<IResourceString> LazyTest1 = new Lazy<IResourceString>(
            () => AddToCultureCache(new ResourceManagerString(""Test1"", ResourceManager, GetDefaultCulture)),
            LazyThreadSafetyMode.PublicationOnly
        );

        public static IResourceString Test1 => LazyTest1.Value;

        #endregion // Test1 
    
        internal static class Test2 
        {
            private static readonly Lazy<IResourceString> LazyFormat = new Lazy<IResourceString>(
                () => AddToCultureCache(new ResourceManagerString(""Test2"", ResourceManager, GetDefaultCulture)),
                LazyThreadSafetyMode.PublicationOnly
            );

            public static IResourceString Format => LazyFormat.Value;
            
            public static IResourceString From(IResourceString prefix, IResourceString p1) => AddToCultureCache(new FormattedResourceString(
                Format, 
                GetDefaultCulture,
                prefix,
                p1
            ));
    
        } 
    
    }
}";

    private readonly string emptyXml = @"<?xml version='1.0' encoding='utf-8'?><root></root>";

    private readonly string invalidXml = @"<?xml version='1.0' encoding='utf-8'?><root></data>";

    [TestMethod]
    public void Test_TryParse_ValidXml()
    {
        // Arrange
        var expectedData = new[] {
            ("Test1", "Value1", string.Empty, "This is a greeting message."),
            ("Test2", "{2}Value2{0}{{1}}", typeof(string).FullName, "2 = prefix"),
            ("Test3", "0xDEADBEEF", typeof(byte[]).FullName, string.Empty)
        };

        // Act
        var result = Parser.TryParse(validXml).Match(
            Some: v => v.Resources,
            None: () => throw new InvalidOperationException()
        );

        // Assert
        CollectionAssert.AreEqual(
            expectedData,
            result.Select(i => (
                i.Name,
                i.Value,
                i.Type.IfNone(string.Empty),
                i.Comment.IfNone(string.Empty)
            )).ToList()
        );
    }

    [TestMethod]
    public void Test_TryParse_EmptyXml()
    {
        // Arrange
        var expectedData = Enumerable.Empty<Resource>();

        // Act
        var isSome = Parser.TryParse(emptyXml).Match(
            Some: _ => true,
            None: () => false
        );

        // Assert
        Assert.IsFalse(isSome);
    }

    [TestMethod]
    public void Test_TryParse_InValidXml()
    {
        // Arrange
        var expectedData = Enumerable.Empty<Resource>();

        // Act
        var isSome = Parser.TryParse(invalidXml).Match(
            Some: _ => true,
            None: () => false
        );

        // Assert
        Assert.IsFalse(isSome);
    }

    [TestMethod]
    public void Test_ToMemberString()
    {
        // Act
        var result = Parser.TryParse(validXml).Match(
            Some: v => CodeSnippetFactory.CreateMemberCodeSnippets(v.Resources),
            None: () => throw new InvalidOperationException()
        );

        // Assert
        Assert.AreEqual(2, result.Count());

        Assert.AreEqual(
          "#region Test1",
          result.Select(i => i.Value.Substring(0, 25).Trim()).ToList()[0]
        );

        Assert.AreEqual(
         "internal static class Test2",
         result.Select(i => i.Value.Substring(0, 45).Trim()).ToList()[1]
       );

    }

    [TestMethod]
    public void Test_ToClassString()
    {
        // Act
        var result = Parser.TryParse(validXml).Match(
            Some: v => CodeSnippetFactory.CreateResourceClassCodeSnippet(
                "TestNameSpace",
                "TestResourceClass",
                CodeSnippetFactory.CreateResourceMangerMemberCodeSnippet("ResourceManager", "TestResourceClass"),
                v.Resources
            ),
            None: () => throw new InvalidOperationException()
        );

        // Assert     
        Assert.AreEqual(
          expectedClass.Trim(),
          result.Value.Trim()
        );
    }
}


