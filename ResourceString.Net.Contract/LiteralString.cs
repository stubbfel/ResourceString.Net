using System;
using System.Globalization;
using System.Threading;

namespace ResourceString.Net.Contract
{
    public sealed class LiteralString : IResourceString
    {
        private Lazy<string> m_Value;

        public string Value => m_Value.Value;

        public static IResourceString Empty { get; } = Factory(string.Empty);
        public static IResourceString Factory(string source)
        {
            return new LiteralString(() => source);
        }

        public LiteralString(Func<string> factory)
        {
            m_Value = new Lazy<string>(
                () => factory?.Invoke() ?? string.Empty,
                LazyThreadSafetyMode.PublicationOnly
            );
        }

        public string GetValue(CultureInfo _) => Value;
    }
}

