using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System;

namespace ResourceString.Net.Contract
{
    public sealed class FormattedResourceString : IResourceString
    {
        private readonly Func<CultureInfo> m_GetDefaultCulture;

        public IResourceString Format { get; }

        public IEnumerable<IResourceString> Parameters { get; }

        public string Value => GetValue(CultureInfo.CurrentCulture);

        public FormattedResourceString(
            IResourceString format,
            Func<CultureInfo>? getDefaultCulture = default,
            params IResourceString[] parameters)
        {
            Format = format ?? new JoinedResourceString(
                LiteralString.Factory(","),
                getDefaultCulture,
                parameters.Select(((p, idx) => LiteralString.Factory("{" + idx + "}"))).ToArray()
            );

            Parameters = parameters;
            m_GetDefaultCulture = getDefaultCulture ?? (() => CultureInfo.CurrentCulture);
        }

        public string GetValue(CultureInfo cultureInfo)
        {
            var format = Format.GetValue(cultureInfo);
            var parameterStrings = Parameters.Select(p => p.GetValue(cultureInfo));
            return string.Format(format, parameterStrings.ToArray());
        }
    }
}

