using System;
using System.Globalization;
using System.Resources;

namespace ResourceString.Net.Contract
{
    public class ResourceManagerString : IResourceString
    {
        private readonly Func<CultureInfo> m_GetDefaultCulture;

        public string Id { get; }

        public ResourceManager Manager { get; }

        public string Value => GetValue(m_GetDefaultCulture());

        public ResourceManagerString(
            string id,
            ResourceManager manager,
            Func<CultureInfo>? getDefaultCulture = default)
        {
            Id = id ?? string.Empty;
            Manager = manager ?? throw new ArgumentNullException(nameof(manager));
            m_GetDefaultCulture = getDefaultCulture ?? (() => CultureInfo.CurrentCulture);
        }

        public string GetValue(CultureInfo cultureInfo)
        {
            return Manager.GetString(Id, cultureInfo);
        }
    }
}

