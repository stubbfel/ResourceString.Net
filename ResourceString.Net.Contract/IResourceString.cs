using System.Globalization;

namespace ResourceString.Net.Contract
{
    public interface IResourceString
    {

        string Value { get; }

        string GetValue(CultureInfo cultureInfo);
    }
}

