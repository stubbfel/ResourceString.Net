﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System;

namespace ResourceString.Net.Contract
{

    public sealed class JoinedResourceString : IResourceString
    {
        private readonly Func<CultureInfo> m_GetDefaultCulture;

        public IResourceString Separator { get; }

        public IEnumerable<IResourceString> Elements { get; }

        public string Value => GetValue(CultureInfo.CurrentCulture);

        public JoinedResourceString(
            IResourceString separator,
            Func<CultureInfo>? getDefaultCulture = default,
            params IResourceString[] elements)
        {
            Separator = separator ?? new LiteralString(
                () => ","
            );

            Elements = elements;
            m_GetDefaultCulture = getDefaultCulture ?? (() => CultureInfo.CurrentCulture);
        }

        public string GetValue(CultureInfo cultureInfo)
        {
            var separator = Separator.GetValue(cultureInfo);
            var elementsStrings = Elements.Select(p => p.GetValue(cultureInfo));
            return string.Join(separator, elementsStrings);
        }
    }
}

