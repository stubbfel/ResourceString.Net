using System;
using System.Collections.Generic;
using System.Globalization;

namespace ResourceString.Net.Contract
{
    public sealed class CultureBasedCachedString : IResourceString
    {
        private readonly Func<CultureInfo> m_GetDefaultCulture;

        private readonly IResourceString m_ResourceString;

        private readonly Dictionary<CultureInfo, string> m_Cache = new Dictionary<CultureInfo, string>();

        public CultureBasedCachedString(
            IResourceString resourceString,
            Func<CultureInfo>? getDefaultCulture = default)
        {
            m_ResourceString = resourceString;
            m_GetDefaultCulture = getDefaultCulture ?? (() => CultureInfo.CurrentCulture);
        }

        public string Value => GetValue(m_GetDefaultCulture());

        public string GetValue(CultureInfo cultureInfo)
        {
            if (m_Cache.TryGetValue(cultureInfo, out var value))
            {
                return value;
            }

            var newValue = m_ResourceString.GetValue(cultureInfo);
            m_Cache[cultureInfo] = newValue;
            
            return newValue;
        }
    }
}

