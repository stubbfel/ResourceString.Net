﻿using ResourceString.Net.Logic.Factories;
using ResourceString.Net.Logic.Parsers.Resx;

var sourceFile = args.First();
var namespaceString = args.Skip(1).FirstOrDefault() ?? "Properties";

var resourceFileName = args.Skip(2).FirstOrDefault() ?? "Resources";
var className = CodeSnippetFactory.TransformToClassName(
    resourceFileName
);

var result = Parser.TryParse(System.IO.File.ReadAllText(sourceFile)).Match(
   Some: v => CodeSnippetFactory.CreateResourceClassCodeSnippet(
       CodeSnippetFactory.TransformToNamespace(namespaceString),
       className,
       CodeSnippetFactory.CreateResourceMangerMemberCodeSnippet(
        $"{namespaceString}.{className}",
        className
        ),
        v.Resources
   ),
   None: () => throw new InvalidOperationException()
);

Console.WriteLine(result.Value.Trim());
