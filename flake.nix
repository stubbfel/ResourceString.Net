{
  description = "";
  inputs.nixpkgs.url = "nixpkgs/nixos-22.11-small";
  outputs = { self, nixpkgs }:
    let
      name = "ResourceString.Net";
      version = "0.0.4";
      supportedSystems =
        [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; });
      scripts = forAllSystems (system: {
        updateFlakeLockFile = nixpkgsFor.${system}.writeScript "update_flake_lock.sh" ''
            nix --experimental-features 'nix-command flakes' flake lock --update-input nixpkgs
            nix --experimental-features 'nix-command flakes' build
          '';

        updateNugetLockFile = nixpkgsFor.${system}.writeScript "update_nuget_lock.sh" ''
            temp_package_folder=$(mktemp -d)
            dotnet restore ResourceString.Net --packages "$temp_package_folder"
            ${nixpkgsFor.${system}.nuget-to-nix}/bin/nuget-to-nix "$temp_package_folder" > ResourceString.Net/deps.nix
            rm -rf "$temp_package_folder"
          '';

        autoTag = nixpkgsFor.${system}.writeScript "auto_tag.sh" ''
          git tag --force v${version}
          git push origin v${version}
        '';
        updateLogicResourceFile = nixpkgsFor.${system}.writeScript "update_logic_resource_file.sh" ''
            dotnet run --project ResourceString.Net.App.Console ResourceString.Net.Logic/Properties/Resources.resx  ResourceString.Net.Logic.Properties > ResourceString.Net.Logic/Properties/Resources.cs.tmp
            mv ResourceString.Net.Logic/Properties/Resources.cs.tmp ResourceString.Net.Logic/Properties/Resources.cs
          '';
        buildAllAssemblies = nixpkgsFor.${system}.writeScript "build_all_assemblies.sh" ''
            # Find all .csproj files below the current folder
            csproj_files=$(find . -name '*.csproj')

            # Create a temporary file to store the number of dependencies for each project
            temp_file=$(mktemp)

            # Loop through each .csproj file and count its number of ResourceString dependencies
            for file in $csproj_files
            do
              count=$(cat "$file" | grep -c "<ProjectReference.*ResourceString")
              depth=$(echo "$file" | tr -cd '/' | wc -c)
              dot_count=$(echo "$file" | tr -cd '.' | wc -c)
              echo "$depth $count $dot_count $file" >> "$temp_file"
            done

            # Sort the projects by their count of dependencies and path depth, in ascending order
            sorted_projects=$(sort -nk1 -nk2 -nk3 -t' ' "$temp_file" | awk '{print $4}')

            # Loop through the sorted projects and build them
            while read -r file
            do
              echo "Building project: $file"
              dotnet build "$file"
            done << EOF
            $sorted_projects
            EOF

            # Remove the temporary file
            rm "$temp_file"

          '';

        cleanAllAssemblies = nixpkgsFor.${system}.writeScript "clean_all_assemblies.sh" ''
            csproj_files=$(find . -name '*.csproj')
            for file in $csproj_files
            do
              dotnet clean "$file"
            done
          '';
        runDefaultApp = nixpkgsFor.${system}.writeScript "run_default_app.sh" ''
          dotnet run --project ResourceString.Net.App.Console $@
        '';
        runDotNet = nixpkgsFor.${system}.writeScript "dotnet.sh" ''
          if [ ! -d "result/share" ]; then
            nix --experimental-features 'nix-command flakes' build 
          fi

          injectable_commands="restore$2 addpackage"          

          for value in $injectable_commands; do
              if [ "$value" = "$1$2" ] || [ "$value" = "$1$3" ]; then
                  dotnet nuget locals all -c                   
                  dotnet $@ -s "result/share;https://api.nuget.org/v3/index.json"
                  exit $?
              fi             
          done

          dotnet $@ 
        '';

        runAllTests = nixpkgsFor.${system}.writeScript "run_all_tests.sh" ''
          dotnet test *.Tests

          echo "# test package intregation"

          echo "## test example app"

          ${scripts.${system}.runDotNet} restore example/Example.App/
          ${scripts.${system}.runDotNet} build example/Example.App/
          output=$(${scripts.${system}.runDotNet} run --project example/Example.App/ --no-build | tr '\n' ' ')

          expected_output="Hello Value1 2Value21{2} "
          if [ "$output" = "$expected_output" ]; then
              echo "Output matches the expected value."
          else
              echo "expected output is:"
              echo $expected_output

              echo "but was:"
              echo $output

              exit 1
          fi

          echo "## test MyTestConsoleApp"

          rm -rf MyTestConsoleApp
          dotnet new console --name MyTestConsoleApp

          echo "<Project Sdk=\"Microsoft.NET.Sdk\">
            <PropertyGroup>
              <OutputType>Exe</OutputType>
              <TargetFramework>net6.0</TargetFramework>
              <ImplicitUsings>enable</ImplicitUsings>
              <Nullable>enable</Nullable>
            </PropertyGroup>

            <PropertyGroup>
              <AdditionalFileItemNames>$(AdditionalFileItemNames);EmbeddedResource</AdditionalFileItemNames>
            </PropertyGroup>

          </Project>
          " > MyTestConsoleApp/MyTestConsoleApp.csproj

          ${scripts.${system}.runDotNet} add MyTestConsoleApp package "System.Resources.Extensions"
          ${scripts.${system}.runDotNet} add MyTestConsoleApp package "ResourceString.Net"

          echo "<?xml version='1.0' encoding='utf-8'?>
          <root>
            <data name='Greetings'>
              <value>Hello {0}</value>
              <comment>0 = name</comment>
            </data>
            <data name='World'>
              <value>World</value>
            </data>
          </root>
          " > MyTestConsoleApp/Resources.resx

          echo "var message = MyTestConsoleApp.Resources.Greetings.From(
              MyTestConsoleApp.Resources.World
          );

          Console.WriteLine(message.Value);
          " > MyTestConsoleApp/Program.cs

          echo "<?xml version='1.0' encoding='utf-8'?>
          <root>
            <data name='Greetings'>
              <value>Hallo {0}</value>
              <comment>0 = name</comment>
            </data>
            <data name='World'>
              <value>Welt</value>
            </data>
          </root>
          " > MyTestConsoleApp/Resources.de.resx

          echo "
          Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo(\"de-DE\");

          Console.WriteLine(message.Value);
          " >> MyTestConsoleApp/Program.cs

          ${scripts.${system}.runDotNet} restore MyTestConsoleApp
          ${scripts.${system}.runDotNet} build MyTestConsoleApp
          output=$(${scripts.${system}.runDotNet} run --project MyTestConsoleApp --no-build | tr '\n' ' ')

          expected_output="Hello World Hallo Welt "
          if [ "$output" = "$expected_output" ]; then
              echo "Output matches the expected value."
          else
              echo "expected output is:"
              echo $expected_output

              echo "but was:"
              echo $output

              exit 1
          fi
        '';
      });

    in rec {
      packages = forAllSystems (system: {
        default = nixpkgsFor.${system}.buildDotnetModule {
          pname = name;
          version = version;

          src = self;

          projectFile = [
            "ResourceString.Net/ResourceString.Net.csproj"
            "ResourceString.Net.App.Console/ResourceString.Net.App.Console.csproj"
          ];

          nugetDeps = ./ResourceString.Net/deps.nix;
          projectReferences = [ ];

          dotnet-sdk = nixpkgsFor.${system}.dotnet-sdk;
          dotnet-runtime = nixpkgsFor.${system}.dotnet-runtime;
          executables = [ "ResourceString.Net.App.Console" ];
          packNupkg = true;
          runtimeDeps = [ ];
        };
      });

      apps = forAllSystems (system: {
        default = {
          type = "app";
          program = "${scripts.${system}.runDefaultApp}";
        };
        devTasks = {
          updateFlakeLock = {
            type = "app";
            program = "${scripts.${system}.updateFlakeLockFile}";
          };
          updateNugetLock = {
            type = "app";
            program = "${scripts.${system}.updateNugetLockFile}";
          };
          autoTag = {
            type = "app";
            program = "${scripts.${system}.autoTag}";
          };
          updateLogicResourceFile = {
            type = "app";
            program = "${scripts.${system}.updateLogicResourceFile}";
          };
          runAllTests = {
            type = "app";
            program = "${scripts.${system}.runAllTests}";
          };
          buildAllAssemblies = {
            type = "app";
            program = "${scripts.${system}.buildAllAssemblies}";
          };
          cleanAllAssemblies = {
            type = "app";
            program = "${scripts.${system}.cleanAllAssemblies}";
          };
          runDotNet = {
            type = "app";
            program = "${scripts.${system}.runDotNet}";
          };
        };
      });

      devShells = forAllSystems (system: {
        default = nixpkgsFor.${system}.mkShell {
          name = "dev-shell";
          packages =
            [ nixpkgsFor.${system}.dotnet-sdk nixpkgsFor.${system}.nixfmt ];
          shellHook = ''
            . ./alias.sh
          '';
        };
      });
    };
}
