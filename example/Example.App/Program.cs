﻿namespace Test
{

    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello");

            var foo = Example.App.Resources.Test1;
            Console.WriteLine(foo.Value);

            var bar = Example.App.Properties.SubProperties.Properties.Test2.From(
                ResourceString.Net.Contract.LiteralString.Factory("1"),
                ResourceString.Net.Contract.LiteralString.Factory("2")
            );

            Console.WriteLine(bar.Value);
        }
    }

}