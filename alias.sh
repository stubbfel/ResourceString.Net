#!/bin/env sh

alias nixe='nix --experimental-features "nix-command flakes"'
alias nulock='nixe run .#devTasks.updateNugetLock'
alias fllock='nixe run .#devTasks.updateFlakeLock'
alias ulock='nulock && fllock'
alias .net='nixe run .#devTasks.runDotNet -- $@'
alias rat='nixe run .#devTasks.runAllTests'