namespace ResourceString.Net.Logic.DomainObjects.Resx;

internal record Resource
{    
    public Resource(string name, string value)
    {
        Name = name ?? string.Empty;
        Value = value ?? string.Empty;
    }
    
    public string Name { get;}

    public Option<string> Type { get; init;}

    public Option<string> Comment { get; init;}

    public string Value { get; }
}
