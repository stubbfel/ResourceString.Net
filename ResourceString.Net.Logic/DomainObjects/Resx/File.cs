using System.Collections.Generic;
using System.Linq;

namespace ResourceString.Net.Logic.DomainObjects.Resx;

internal record File
{
    public File(IEnumerable<Resource> resources)
    {
        Resources = new Seq<Resource>(
            resources?.Where(o => o != null) ?? Enumerable.Empty<Resource>()
        );
    }

    public IEnumerable<Resource> Resources { get; }    
}
