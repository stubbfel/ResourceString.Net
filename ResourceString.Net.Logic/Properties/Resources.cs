using ResourceString.Net.Contract;
using System;
using System.Globalization;
using System.Resources;
using System.Threading;

namespace ResourceString.Net.Logic.Properties
{
    internal static class Resources
    {

        #region ResourceManager 

        private static readonly Type _Type = typeof(Resources);

        private static readonly Lazy<ResourceManager> _ResourceManager = new Lazy<ResourceManager>(
            () => new ResourceManager("ResourceString.Net.Logic.Properties.Resources" ?? string.Empty, _Type.Assembly),
            LazyThreadSafetyMode.PublicationOnly
        );

        public static ResourceManager ResourceManager => _ResourceManager.Value;

        private static CultureInfo GetDefaultCulture()
        {
            return CultureInfo.CurrentCulture;
        }

        private static IResourceString AddToCultureCache(IResourceString source)
        {
            return new CultureBasedCachedString(source, GetDefaultCulture);
        }

        #endregion // ResourceManager


        internal static class ResourceStringMembers
        {
            private static readonly Lazy<IResourceString> LazyFormat = new Lazy<IResourceString>(
                () => AddToCultureCache(new ResourceManagerString("ResourceStringMembers", ResourceManager, GetDefaultCulture)),
                LazyThreadSafetyMode.PublicationOnly
            );

            public static IResourceString Format => LazyFormat.Value;

            public static IResourceString From(IResourceString resourceId, IResourceString resourceManagerPropertyName) => AddToCultureCache(new FormattedResourceString(
                Format,
                GetDefaultCulture,
                resourceId,
                resourceManagerPropertyName
            ));

        }

        internal static class ResourceFormatClassMembers
        {
            private static readonly Lazy<IResourceString> LazyFormat = new Lazy<IResourceString>(
                () => AddToCultureCache(new ResourceManagerString("ResourceFormatClassMembers", ResourceManager, GetDefaultCulture)),
                LazyThreadSafetyMode.PublicationOnly
            );

            public static IResourceString Format => LazyFormat.Value;

            public static IResourceString From(IResourceString resourceId, IResourceString resourceManagerPropertyName, IResourceString fromMethodDefinition) => AddToCultureCache(new FormattedResourceString(
                Format,
                GetDefaultCulture,
                resourceId,
                resourceManagerPropertyName,
                fromMethodDefinition
            ));

        }

        internal static class ResourceFormatClassFromMethod
        {
            private static readonly Lazy<IResourceString> LazyFormat = new Lazy<IResourceString>(
                () => AddToCultureCache(new ResourceManagerString("ResourceFormatClassFromMethod", ResourceManager, GetDefaultCulture)),
                LazyThreadSafetyMode.PublicationOnly
            );

            public static IResourceString Format => LazyFormat.Value;

            public static IResourceString From(IResourceString fromMethodSignature, IResourceString parameterNames) => AddToCultureCache(new FormattedResourceString(
                Format,
                GetDefaultCulture,
                fromMethodSignature,
                parameterNames
            ));

        }

        internal static class ResourcesClassTemplate
        {
            private static readonly Lazy<IResourceString> LazyFormat = new Lazy<IResourceString>(
                () => AddToCultureCache(new ResourceManagerString("ResourcesClassTemplate", ResourceManager, GetDefaultCulture)),
                LazyThreadSafetyMode.PublicationOnly
            );

            public static IResourceString Format => LazyFormat.Value;

            public static IResourceString From(IResourceString ns, IResourceString className, IResourceString resourceManagerRegion, IResourceString resourceRegions) => AddToCultureCache(new FormattedResourceString(
                Format,
                GetDefaultCulture,
                ns,
                className,
                resourceManagerRegion,
                resourceRegions
            ));

        }

        internal static class ResourceManagerMemberTemplate
        {
            private static readonly Lazy<IResourceString> LazyFormat = new Lazy<IResourceString>(
                () => AddToCultureCache(new ResourceManagerString("ResourceManagerMemberTemplate", ResourceManager, GetDefaultCulture)),
                LazyThreadSafetyMode.PublicationOnly
            );

            public static IResourceString Format => LazyFormat.Value;

            public static IResourceString From(IResourceString className, IResourceString resourceManagerTypeName) => AddToCultureCache(new FormattedResourceString(
                Format,
                GetDefaultCulture,
                className,
                resourceManagerTypeName
            ));

        }

        #region DefaultPropertyName_ResourceManager 

        private static readonly Lazy<IResourceString> LazyDefaultPropertyName_ResourceManager = new Lazy<IResourceString>(
            () => AddToCultureCache(new ResourceManagerString("DefaultPropertyName_ResourceManager", ResourceManager, GetDefaultCulture)),
            LazyThreadSafetyMode.PublicationOnly
        );

        public static IResourceString DefaultPropertyName_ResourceManager => LazyDefaultPropertyName_ResourceManager.Value;

        #endregion // DefaultPropertyName_ResourceManager 

    }
}