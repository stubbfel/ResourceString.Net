using ResourceString.Net.Logic.DomainObjects.Resx;
using System.Linq;
using System.Xml;

namespace ResourceString.Net.Logic.Parsers.Resx;

internal static class Parser
{
    public static Option<File> TryParse(string xmlString)
    {
        var doc = new XmlDocument();

        try
        {
            doc.LoadXml(xmlString);
        }
        catch
        {
            return Option<File>.None;
        }

        var resources = doc.SelectNodes("descendant::data").OfType<XmlElement>().Select((i, _) =>
        {
            var name = i.GetAttribute("name");
            var type = i.GetAttribute("type");
            var comment = i.SelectSingleNode("descendant::comment")?.InnerXml ?? string.Empty;
            var value = i.SelectSingleNode("descendant::value")?.InnerXml ?? string.Empty;

            return new Resource(name, value)
            {
                Type = string.IsNullOrWhiteSpace(type)
                    ? Option<string>.None
                    : Option<string>.Some(type),
                Comment = string.IsNullOrWhiteSpace(comment)
                    ? Option<string>.None
                    : Option<string>.Some(comment)
            };
        }).ToArray();

        return resources.Any()
           ? Option<File>.Some(new File(resources))
           : Option<File>.None;
    }
}
