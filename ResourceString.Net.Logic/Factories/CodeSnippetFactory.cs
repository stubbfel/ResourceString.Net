using ResourceString.Net.Logic.DomainObjects.Resx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ResourceString.Net.Logic.Factories;

internal static class CodeSnippetFactory
{
    public static IResourceString CreateResourceMangerMemberCodeSnippet(
        string resourceName,
        string className)
    {
        return Properties.Resources.ResourceManagerMemberTemplate.From(
            LiteralString.Factory(resourceName),
            LiteralString.Factory(className)
        );
    }

    public static IEnumerable<IResourceString> CreateMemberCodeSnippets(IEnumerable<Resource> resources)
    {
        return CreateMemberCodeSnippets(
            resources,
            Properties.Resources.DefaultPropertyName_ResourceManager
        );
    }

    public static IEnumerable<IResourceString> CreateMemberCodeSnippets(
        IEnumerable<Resource> resources,
        IResourceString resourceManagerName)
    {
        if (resources is null)
        {
            return Enumerable.Empty<IResourceString>();
        }

        var stringResources = resources.Where(r => r.Type.Match(
            v => typeof(string).IsAssignableFrom(Type.GetType(v.Trim(), false, true)),
            () => true
        ));

        return stringResources.Select(r =>
        {
            var openBraces = r.Value
                .Replace("{{", string.Empty)
                .Replace("{{", string.Empty)
                .Split('{')
                .Skip(1)
                .ToArray();

            if (openBraces.Any())
            {
                var formatNames = new System.Collections.Generic.HashSet<string>(
                    openBraces.Select(b => b.Split('}').First())
                );

                var resolveParameterNames = r.Comment.Match(
                    c => c.Split(',')
                        .Select(str => str.Split('='))
                        .Where(parts => parts.Count() == 2)
                        .Select(parts => (
                            parts.First().Trim().Trim('{', '}').Split(' ').First(),
                            parts.Last().Trim().Split(' ').First()
                        )).Where(t => uint.TryParse(t.Item1, out var _))
                        .GroupBy(t => t.Item1)
                        .ToDictionary(k => k.Key, v => v.First().Item2),
                    () => new Dictionary<string, string>()
                );

                var parameterNames = formatNames.Select(
                    n => resolveParameterNames.TryGetValue(n.Trim(), out var paramName)
                        ? paramName.Substring(0, 1).ToLower() + paramName.Substring(1)
                        : uint.TryParse(n, out var value) ? $"p{value + 1}" : n
                ).ToArray();

                var from = Properties.Resources.ResourceFormatClassFromMethod.From(
                    new JoinedResourceString(
                        LiteralString.Factory(", "),
                        elements: parameterNames.Select(n => LiteralString.Factory($"{nameof(IResourceString)} {n}")).ToArray()
                    ),
                    new JoinedResourceString(
                        LiteralString.Factory($",{System.Environment.NewLine}                "),
                        elements: parameterNames.Select(n => LiteralString.Factory($"{n}")).ToArray()
                    )
                );
                return Properties.Resources.ResourceFormatClassMembers.From(
                    LiteralString.Factory(r.Name), resourceManagerName, from
                );
            }
            return Properties.Resources.ResourceStringMembers.From(
                LiteralString.Factory(r.Name), resourceManagerName
            );
        });
    }

    public static IResourceString CreateResourceClassCodeSnippet(
        string namespaceString,
        string resourceClassName,
        IResourceString resourceManagerSnippet,
        IEnumerable<IResourceString> memberSnippets)
    {
        return Properties.Resources.ResourcesClassTemplate.From(
            LiteralString.Factory(namespaceString),
            LiteralString.Factory(resourceClassName),
            resourceManagerSnippet,
            new JoinedResourceString(
                LiteralString.Empty,
                elements: memberSnippets?.ToArray() ?? Array.Empty<IResourceString>()
            )
        );
    }

    public static IResourceString CreateResourceClassCodeSnippet(
       string namespaceString,
       string resourceClassName,
       IResourceString resourceManagerSnippet,
       IEnumerable<Resource> resources
       )
    {
        return CreateResourceClassCodeSnippet(
            namespaceString,
            resourceClassName,
            resourceManagerSnippet,
            CreateMemberCodeSnippets(resources)
        );
    }

    public static string TransformToClassName(string input)
    {
        // Remove invalid characters and replace them with underscores
        string sanitizedInput = Regex.Replace(
            input?.Trim() ?? string.Empty,
            @"[^a-zA-Z0-9_]", "_"
        );

        // Remove leading digits if the string starts with a digit
        return Regex.Replace(sanitizedInput, @"^\d+", "");
    }

    public static string TransformToNamespace(string input)
    {
        // Remove invalid characters and replace them with dots
        string sanitizedInput = Regex.Replace(
            input?.Trim() ?? string.Empty,
            @"[^a-zA-Z0-9_.]", "."
        );

        // Remove leading dots if present
        return sanitizedInput.TrimStart('.');
    }
}